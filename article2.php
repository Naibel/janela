<!doctype html>
<html>
<?php
require_once '_head.html';
?>
<body>
<?php
require_once '_header.html'
?>

<?php
require_once '_header_slide.html';
?>

<?php
require_once '_menu_slide.html';
?>

<div class="content">
    <div class="article">
        <div class="header_article" style="background-image: url(images/art2_img1.jpg);">
            <span class="titre_article">Une maison invisible</span><br/>
            <span class="redac_article">Par Aude De La Conté</span><br/>
            <span>le 8 novembre 2015</span><br/><br/>
            <span class="redac_article" style="font-size: 20px;">Durée : 5 minutes</span>
        </div>
        <div class="inner_article">
				<span class="chapeau">
					Sous ce gazon parfait et cette "boîte" minimaliste se cachent d'insoupçonnables espaces au décor élégant et chaleureux, imaginé par les décorateurs Jacques Bec et Artur Miranda.
					<br/><br/>
				</span>

            <p>
                À première vue, c’est une modeste boîte de verre posée sur le gazon d’une colline. Dans cette région du nord du Portugal, l’herbe est plus verte qu’ailleurs, engraissée par les pluies qu’apporte l’océan. Il faut arriver à pied et se laisser surprendre. Pousser la petite porte du cube, admirer la vue à travers les deux trouées de verre, fouler le sol recouvert d’une moquette à motif camouflage, puis descendre l’escalier. Alors surgit sous nos yeux une autre demeure, bien plus vaste. Éclaboussée de lumière, car le terrain en pente a permis le percement de patios et de larges baies vitrées. Les pièces sont vastes mais pas minimalistes. « Les volumes et les proportions pourraient être ceux d’une fondation d’art contemporain, mais ici c’est une maison », précise Jacques Bec qui, avec Artur Miranda, cofondateur de l’agence Oitoemponto, a décoré la maison.
                <br/><br/>
            </p>

            <span class="sous-titre">Ludique et sophistiqué</span><br/><br/>

            <p>
                Travaillant main dans la main avec l’architecte et le paysagiste dès la conception, les deux décorateurs avaient pour objectif de donner au lieu un aspect chaleureux et de conjuguer une maison hautement technologique – rideaux, chauffage, musique, entrée se commandent d’un iPhone – avec une vie de famille et deux enfants. Leur secret ? Les mélanges du mobilier vintage et des créations qu’ils font réaliser spécialement par des artisans.<br/>
                Leurs chantiers les conduisent dans le monde entier, de Londres à Paris, de Los Angeles ou Luanda à São Paulo. Partout ils chinent et rapportent leurs trouvailles qu’ils stockent dans un hangar non loin de leur agence, installée dans un quartier chic et calme de Porto. Partout ils regardent les matériaux – bois en Afrique, pierres au Brésil, céramiques ou marbre au Portugal –, les œuvres ou objets d’art contemporain des galeries européennes, et même le détail d’un système d’accrochage de rideaux aux États-Unis.<br/>
                Le succès du duo est sans aucun doute lié au degré d’attention que les deux partenaires portent à chaque détail, à la sophistication de leurs intérieurs, dont la réalisation se fait toujours en concertation avec les propriétai­res. Leur énergie stimulante, leur instinct pour faire se confronter œuvres, objets et mobilier, la dimension ludique qu’ils n’oublient jamais, y sont aussi pour quelque chose. Car pour eux les maisons sont d’abord « des machines à vivre » et le plus grand compliment ce sont, disent-ils, les yeux des enfants qui brillent.
            </p>

            <img src="images/art2_img2.jpg"><div class="ligne"></div><div>
                <span class="legende">Dans la salle à manger, sur le mur en marbre Kenya Black se détache une console aux hérons en bronze signée Jacques Duval-Brasseur. C’était à l’origine une grande table de salle à manger dont les décorateurs ont redimensionné le plateau en verre. Lampe en laque et laiton de Jean-Claude Mahey, tapis en laine et soie (Diurne).</span></div><br/>

            <img src="images/art2_img3.jpg"><div class="ligne"></div><div>
                <span class="legende">Le vaste salon, situé sous le cube de l’entrée, est éclairé par les larges baies percées à flanc de colline et par un puits de lumière. L’ouverture dans le plafond est masquée par un plateau suspendu et peint en noir qui souligne le volume blanc et fait écho au tableau Black Earth d’Anselm Reyle, au fond. Le mobilier est un mélange de vintage, fauteuils Vela Bassa des années 1970 de Saporiti, et de créations Oitoemponto, comme la table basse Prisma en bronze et marqueterie de palissandre et le canapé Aspen. Au mur, un miroir d’Hervé van der Straeten domine la cheminée.</span></div><br/>

            <img src="images/art2_img4.jpg"><div class="ligne"></div><div>
                <span class="legende">La « family room », avec ses murs tapissés d’ébène de Macassar, est la pièce la plus intime. Au fond, une sculpture en acier nickelé de James Prestini. Le fauteuil blanc Elda de Joe Colombo dialogue avec le guéridon Épines en bronze et marbre d’Hervé van der Straeten, tandis que la table basseAtenas est un projet des décorateurs. Les voilages ont un système d’accroches américain qui permet un plissé zigzag régulier.</span></div><br/>

            <img src="images/art2_img5.jpg"><div class="ligne"></div><div>
                <span class="legende">Le coin petit déjeuner, dans la cuisine, donne l’impression de faire partie du jardin avec sa baie vitrée entièrement escamotable dans le mur. La table est un travail italien des années 1950 en palissandre et laiton avec plateau en marbre. Les chaises Sculpta (1966) de Chromcraft ont fait partie, avec un autre revêtement, du décor de l’épisode pilote de la série Star Strek.</span></div><br/>

            <img src="images/art2_img6.jpg"><div class="ligne"></div><div>
                <span class="legende">Dans la salle à manger, le triptyque Die Gralsschlacht Isislied de Jonathan Meese domine le fond de la pièce. Sous le lustre en laiton doré et verre, travail italien des années 1970, la table de salle à manger Root en bronze de Roberto Kuo a changé de taille car les décorateurs lui ont substitué un plateau plus grand qu’ils ont fait réaliser en laque blanche gravée. Les chaises Pamplona en palissandre et acier ont été dessinées par l’Italien Augusto Savini pour Pozzi en 1965.</span></div><br/>

            <img src="images/art2_img7.jpg"><div class="ligne"></div><div>
                <span class="legende">La piscine fait face à la large terrasse du salon. Entourée de granit, elle est tapissée d’un carrelage traditionnel portugais en céramique verte donnant l’impression de refléter le gazon. Chaises longues de Richard Schultz (Knoll). Au fond, sculpture Poulette de Jean-Claude Moure dit Remou.</span></div><br/>


            <span>Source : <a href="http://www.admagazine.fr/decoration/articles/au-portugal-une-maison-invisible/1592#fuIZjg6Wzczm2oXA.99">AD Magazine</span></a>

        </div>
    </div>
</div>

<?php
require_once '_footer.html';
?>

<?php
require_once '_copyright.html';
?>
<?php
require_once '_scripts.html';
?>
</body>
</html>