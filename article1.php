<!doctype html>
<html>
<?php
require_once '_head.html';
?>
<body>
<?php
require_once '_header.html'
?>

<?php
require_once '_header_slide.html';
?>

<?php
require_once '_menu_slide.html';
?>

<div class="content">
	<div class="article">
		<div class="header_article" style="background-image: url(images/port_cont.jpg);">
			<span class="titre_article">Un Portugal très contemporain</span><br/>
			<span class="redac_article">Par Christian Simenc</span><br/>
			<span>le 21 août 2015</span><br/><br/>
			<span class="redac_article" style="font-size: 20px;">Durée : 5 minutes</span>
		</div>
		<div class="inner_article">
				<span class="chapeau">
					Confit dans ces traditions, le Portugal ? Pas du tout. Côté architecture notamment, ce petit pays a beaucoup à montrer. Nous avons balisé la route de ses plus belles créations, de Lisbonne à Porto. Un parcours bluffant.
					<br/><br/>
				</span>

				<p>
					On a toujours tendance à réduire le Portugal à une terre de fortes traditions, coincée entre fado et azulejo. Que nenni ! Le pays sait aussi produire le meilleur de la contemporanéité, à commencer par l’architecture. Et qui posé les fondements de cette modernité ? Tout le monde, ici, s’accorde sur un nom : Fernando Tavora (1923-2005). Mais celui qui, le premier, hissa la renommée de l’architecture portugaise au-delà des frontières lusitaniennes est son « cadet », pour ne pas dire son fils spirituel : Alvaro Siza, 80 ans ce mois-ci, lequel a décroché, en 1992, le prestigieux Pritzker Prize, considéré comme le Nobel d’architecture.
					<br/><br/>
					Dans ce club très fermé des stars de l’architecture planétaire, il a d’ailleurs été rejoint, en 2011, par son compatriote Eduardo Souto de Moura, 60 ans. Deux Pritzker Prize au compteur de ce « petit » pays – un peu plus de dix millions d’habitants –, c’est autant que la France, le Brésil ou l’Italie, et plus que le Mexique, l’Allemagne et l’Espagne, preuve de la qualité intrinsèque de ses maîtres d’œuvre. Depuis l’an 2000, la production architecturale au Portugal a connu une croissance sans précédent. En voici quelques pièces maîtresses, à travers un road trip qui, du sud au nord, de Lisbonne à Porto, part à la découverte des vedettes et/ou figures montantes de l’architecture lusitanienne.
					<br/>
				</p>

				<span class="sous-titre">La tour de contrôle du port de Lisbonne, à Algés, par Gonçalo Byrne</span><br/><br/>
				<img src="images/port_cont_2.jpg"><div class="ligne"></div><div><span class="legende">© Vincent Leroux</span></div><br/>

				<p>
					C’est, en quelque sorte, la version contemporaine et lisboète de la… tour de Pise. Sauf que, matériaux et technologies aidant, l’édifice s’incline plus encore que son ancêtre italien. Sans doute cherche-t-elle ainsi à se mirer dans les eaux sombres du Tage… La tour de contrôle du trafic maritime du port de Lisbonne, conçue en 2001 par l’architecte Gonçalo Byrne, opère un processus de dématérialisation progressive : d’abord, un socle de pierre blanche, puis, sur cinq étages, des plaques de cuivre dont l’état d’oxydation – du vert clair au brun anthracite – diffère selon que les plans sont droits ou inclinés, enfin, trois étages entièrement vitrés. Avec ses 38 mètres de hauteur et son inclinaison à outrance, l’édifice se révèle un parfait signal visible de toute la côte alentour, un rituel de l’entrée dans la ville par le fleuve.
					<br/><br/>
				</p>

				<span class="sous-titre">Le musée Paula Rego, à Cascais, par Eduardo Souto de Moura</span><br/><br/>
				<img src="images/port_cont_3.jpg"><div class="ligne"></div><div><span class="legende">© Vincent Leroux</span></div><br/>

				<p>
					Ces deux hauts puits de lumière pyramidaux en forme de cheminées primitives détonnent dans un paysage côtier fait d’architectures blanches. Ils sont, selon leur auteur, l’architecte Eduardo Souto de Moura, tout à la fois « des références à la cuisine du monastère d’Alcobaça, à certaines maisons de l’architecte portugais Raul Lino et à des dessins d’Étienne-Louis Boullée ». Leur force : évoquer quelque archétype intemporel de l’iconographie urbaine comme la tour, le phare et autre silo. Le béton rouge, choix radical et univoque, est en opposition chromatique au vert de la forêt alentour.
					Ouvert en 2009, ce musée est entièrement dédié à l’immense artiste portugaise Paula Rego, née le 26 janvier 1935 à Lisbonne, mais installée depuis de nombreuses années en Angleterre.
					<br/><br/>
				</p>

				<span class="sous-titre">Le site archéologique Praça Nova du château de São Jorge, à Lisbonne, par João Luís Carrilho da Graça</span><br/><br/>
				<img src="images/port_cont_4.jpg"><div class="ligne"></div><div><span class="legende">© Vincent Leroux</span></div><br/>

				<p>
					En 1996, une campagne de fouilles archéologiques débute dans l’enceinte même du château et met au jour les traces de plusieurs périodes d’installations successives : des habitations datant de l’âge du fer – du viie au iiie siècle avant J.-C. –, des maisons de l’époque médiévale musulmane – xie et xiie siècles –, enfin, des restes du palais des Comtes de Santiago – du xve au xviiie siècle. Ce Nucleo Arqueologico jusqu’alors protégé est, depuis 2010, révélé au public grâce à un projet subtil et élégant signé par l’architecte João Luís Carrilho da Graça (JLCG Arquitectos). Le plus intriguant des trois sites est le « quartier » islamique, préservé par une vaste structure de béton blanc et d’acier qui ne touche terre qu’en six points seulement. L’effet est fabuleux : les parois contemporaines semblent en lévitation au-dessus des vestiges antiques.
					<br/><br/>
				</p>

				<span class="sous-titre">La chapelle de Santa Filomena, à Netos, par Pedro Maurício Borges</span><br/><br/>
				<img src="images/port_cont_5.jpg"><div class="ligne"></div><div><span class="legende">© Vincent Leroux</span></div><br/>

				<p>
					Quasiment à mi-chemin entre Lisbonne et Porto, à une encablure de la cité balnéaire de Figueira da Foz, s’est posé un ovni : la Capela de Santa Filomena, dessinée par l’architecte Pedro Maurício Borges et consacrée en 2009. Nous sommes au centre de Netos, petit village de campagne comme on en trouve une ribambelle au Portugal. Taillé tel un diamant, l’édifice, d’une extrême simplicité, est on ne peut plus radical : d’abord, sa forme spectaculaire, immaculée et anguleuse, ensuite son intérieur sans dorures ni vitraux, accompagné d’un mobilier rigoureux. Autre originalité : l’immense fenêtre qui, de l’extérieur, invite le regard à pénétrer dans l’étrange bâtiment, et, de l’intérieur, place, aux premières loges, le petit théâtre du quotidien. Spirituelle modernité !
					<br/><br/>
				</p>

				<span class="sous-titre">La Casa da Musica, à Porto, par Rem Koolhaas</span><br/><br/>
				<img src="images/port_cont_6.jpg"><div class="ligne"></div><div><span class="legende">© Vincent Leroux</span></div><br/>

				<p>
					C’est l’exception qui confirme la règle, puisque son architecte n’est pas portugais, mais l’édifice est indéniablement l’un des projets phares de ces dernières années au Portugal. La Casa da Musica est l’œuvre de la star néerlandaise Rem Koolhaas. Planté telle une météorite à l’entrée de la célèbre Avenida da Boavista, au milieu d’une place minérale où les skateurs s’en donnent à cœur joie, ce lieu du spectacle – neuf étages, deux salles de concerts – a été inauguré en 2005. L’intérieur juxtapose avec subtilité les matériaux : béton brut de décoffrage dans les circulations et, dans la grande salle, panneaux de bois à la facture très modeste mais orné de feuilles d’or qui dessinent des zébrures du plus bel effet. Un salon d’honneur rappelle la tradition ornementale portugaise de l’azulejo, mais de manière réactualisée. Le bleu de Delft, plus profond, y a d’ailleurs remplacé le bleu lusitanien.
					<br/><br/>
				</p>

				<span class="sous-titre">La Piscine des Marées, à Leçà da Palmeira, par Álvaro Siza</span><br/><br/>
				<img src="images/port_cont_7.jpg"><div class="ligne"></div><div><span class="legende"s>© Vincent Leroux</span></div><br/>

				<p>
					Comme les dévots vont à Fatima, les pieux fans d’architecture se doivent de passer par Leçà da Palmeira, source de la modernité portugaise. Álvaro Siza y a dessiné, en 1966, une Piscina de Agua Salgada, autrement dit une piscine d’eau de mer, en prise directe avec l’océan atlantique. Un monument. Pour ne pas offusquer la mer, Siza se fait humble, s’immisce entre les affleurements rocheux sur la pointe des murs. À aucun moment, sa construction n’altère ce site à l’indéniable beauté naturelle. Au contraire, elle le révèle. En contrebas, tôt le matin, les surfers chevronnés viennent dompter les rouleaux de l’océan.
					Désigné, l’an passé, Lion d’or pour l’ensemble de son œuvre à la 13e Biennale d’architecture de Venise, Álvaro Siza est le plus célèbre des maîtres d’œuvre portugais vivants et aujourd’hui, sans aucun doute, l’un des plus grands architectes du monde.
					<br/><br/>
				</p>

				<span>Source : <a href="http://www.admagazine.fr/architecture/balade/articles/un-portugal-trs-contemporain/14091#oRafrx5XUHMSdwHB.99">AD Magazine</span></a>

		</div>
	</div>
</div>

<?php
require_once '_footer.html';
?>

<?php
require_once '_copyright.html';
?>
<?php
require_once '_scripts.html';
?>
</body>
</html>