<!doctype html>
<html>
<?php
require_once '_head.html';
?>
<body>
<?php
require_once '_header.html'
?>

<?php
require_once '_header_slide.html';
?>

<?php
require_once '_menu_slide.html';
?>

<div class="content">
	<div class="article">
		<div class="header_article" style="background-image: url(images/3.jpg);">
			<span class="titre_article">Titre de l'article</span><br/>
			<span class="redac_article">Par Machin Chose</span><br/>
			<span>Le 16 mars 2016</span><br/><br/>
			<span class="redac_article" style="font-size: 20px;">Durée : 5 minutes</span>
		</div>
		<div class="inner_article">
				<span class="chapeau">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin vehicula vulputate velit vel facilisis. Cras ut dolor ultrices, cursus quam scelerisque, porttitor metus. Nullam sed est in sem porta posuere in ut nisl. Donec a orci fringilla, sodales leo nec, venenatis orci. Pellentesque scelerisque hendrerit libero eu dapibus. Suspendisse egestas luctus enim, in tristique nisl commodo id. Vestibulum auctor enim sit amet lacinia ultricies. Donec at dolor blandit, rutrum nibh molestie, gravida mauris. Morbi ac nisl vitae risus finibus posuere vel faucibus elit. Praesent sit amet dignissim sem, vel tempor massa.
					<br/><br/>
				</span>

				<span>
					<span class="sous-titre">Sous-titre 1</span>
					<br/>
					<p>
						Vivamus dolor eros, suscipit nec aliquam ac, tempor a nulla. Praesent tempor tempus ex eget ullamcorper. Proin blandit orci ac scelerisque tincidunt. Sed vel est vitae leo commodo fringilla et egestas eros. Vestibulum vel tincidunt erat. Proin ipsum massa, molestie quis libero in, auctor suscipit diam. Integer felis arcu, eleifend vitae vestibulum ut, viverra nec mauris. Pellentesque vestibulum justo hendrerit ornare vestibulum. In enim purus, eleifend id orci in, sagittis varius nibh. Sed leo nulla, ullamcorper id vehicula eu, luctus et justo. Nulla congue dignissim arcu, sit amet tempus lorem. Nulla accumsan purus eget mauris tristique pharetra eget eu sem. Duis euismod lorem vel mollis viverra. Sed dictum blandit cursus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
						<br/>
						Nunc interdum nisl at tellus commodo, a laoreet arcu porta. Curabitur ultrices et tellus et vulputate. Ut mollis est non molestie vestibulum. Nulla auctor lorem a molestie aliquet. Phasellus neque neque, feugiat at urna ut, ullamcorper lobortis dolor. Mauris auctor justo eget augue sagittis, ut elementum leo imperdiet. Pellentesque fringilla est eget odio cursus, non cursus elit eleifend. Quisque consectetur sodales diam, sed sodales risus gravida eu.
						<br/>
						<img src="images/3.jpg">
					</p>

					<p>
						Sed vestibulum pellentesque dolor, id suscipit lectus pellentesque ultricies. Etiam vel tristique velit, ut consectetur lorem. Proin nec felis a augue lobortis lacinia vulputate in mi. Pellentesque vel massa in orci malesuada varius. Praesent urna eros, laoreet vel nulla ac, mollis tincidunt sem. Nulla quis metus sem. Sed rutrum egestas faucibus.
						<br/>
						Pellentesque molestie interdum justo, et pretium metus ultrices eget. Maecenas in diam pharetra, pellentesque nisl eget, mollis libero. Nunc euismod magna at nisi rhoncus imperdiet. Fusce sollicitudin lorem sit amet lectus condimentum, non dapibus metus facilisis. Phasellus porttitor diam leo, nec ultricies erat tristique eu. Aenean eu lorem a enim cursus rhoncus. Maecenas turpis libero, sodales sollicitudin tortor vitae, vestibulum auctor nunc. Praesent finibus purus ut sollicitudin congue. Sed sollicitudin varius nulla, sodales auctor nulla facilisis eget. Aliquam vel felis a turpis malesuada suscipit vitae nec nisl.
						<br/><br/>
					</p>



					<span class="sous-titre">Sous-titre 1</span>
					<br/>
					<p>
						Nam consectetur erat sapien, sit amet vulputate justo sodales vitae. Nunc tempus elit eget pellentesque elementum. Integer mauris nisl, blandit sit amet varius in, efficitur sed quam. Etiam cursus magna nibh, et aliquet neque facilisis ac. Fusce sodales quis tortor quis maximus. Maecenas vehicula turpis sed lectus vulputate efficitur. In ultrices felis eget dapibus laoreet. Nullam accumsan lectus in sapien placerat convallis. Nullam sed turpis vulputate diam vestibulum pharetra. Praesent et ante cursus, sollicitudin massa consectetur, congue eros. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum sed risus tincidunt erat cursus convallis sit amet faucibus turpis. Quisque a ante sed felis ultrices hendrerit sit amet id purus. In sed urna bibendum, varius augue eget, vestibulum mi. Proin eu semper turpis.
						<br/><br/>
						Praesent vel dolor non est tempus sagittis nec ac lorem. Nunc ultricies varius ullamcorper. Integer vehicula, lacus vitae interdum dictum, dolor dolor consequat neque, sit amet vestibulum enim felis et libero. Maecenas viverra, nunc in viverra mattis, ex nulla aliquet nisi, sed venenatis nulla felis quis mi. Mauris vel lorem vitae quam vestibulum sagittis. Mauris dignissim odio vitae dolor pulvinar, ut suscipit nisl accumsan. Vestibulum lobortis tellus ut lorem tincidunt, non luctus dolor fermentum. Morbi laoreet libero maximus pellentesque dictum. Suspendisse id interdum nunc, sit amet suscipit magna. Curabitur euismod dolor sed ligula pretium, a finibus elit luctus.
						<br/><br/>
					</p>



					<p>
						<img src="images/2.jpg">
						Nam ullamcorper libero eget ante pharetra viverra. Mauris pretium justo quis diam egestas, et semper ex gravida. Donec sit amet diam sit amet diam condimentum placerat sed ut mauris. Vivamus facilisis commodo orci, sed euismod quam dictum vel. Vestibulum a tempor neque. Sed ac commodo tortor, nec euismod justo. Fusce porta dapibus mauris sed ultricies. Curabitur blandit vulputate venenatis. Curabitur sed risus a mi varius interdum quis nec elit. Etiam fringilla ligula et euismod facilisis. Maecenas sit amet elit mauris. Aliquam hendrerit metus eros, at aliquet orci accumsan nec. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris vestibulum urna sapien, in posuere tortor efficitur quis. Quisque facilisis ex vel elit commodo, eget iaculis purus vestibulum.
						<br/><br/>
						Donec vel nulla pulvinar orci sollicitudin viverra. Donec sollicitudin consequat tincidunt. Nulla placerat placerat ornare. Donec in molestie metus. Suspendisse ullamcorper odio vel venenatis fermentum. Praesent dolor sapien, ultrices quis elementum nec, imperdiet vitae dolor. Curabitur tincidunt tempus elit nec pulvinar. Pellentesque sit amet porttitor libero. Morbi tellus ipsum, accumsan vitae facilisis vel, rhoncus vitae augue. Nunc eleifend aliquet tempor. Maecenas eget nisi leo. Cras bibendum aliquam luctus. Nam sed dui at urna accumsan hendrerit. Sed luctus dolor a magna rutrum molestie.
						<br/><br/>
						nteger diam sapien, semper sit amet sem ac, pretium malesuada sapien. Aliquam euismod pellentesque justo a efficitur. Fusce ipsum dolor, faucibus et nisi quis, venenatis aliquam erat. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Pellentesque ullamcorper fermentum aliquet. Cras dictum augue eget urna tempor, eget finibus sapien convallis. Sed eu ligula purus. Pellentesque condimentum et erat sit amet posuere. Fusce in magna non ante elementum tincidunt. Proin ac feugiat enim, at volutpat est. Vestibulum vitae venenatis est. Ut eget viverra mi. Praesent laoreet risus eu justo sollicitudin, ac posuere sapien dictum.
						<br/>
					</p>

				</span>
		</div>
	</div>
</div>

<?php
require_once '_footer.html';
?>

<?php
require_once '_copyright.html';
?>
<?php
require_once '_scripts.html';
?>
</body>
</html>