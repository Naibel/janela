<!doctype html>
<html>
<?php
require_once '_head.html';
?>
<body>
<?php
require_once '_header.html'
?>

<?php
require_once '_header_slide.html';
?>

<?php
require_once '_menu_slide.html';
?>

<div class="content">
    <div id="first_part">
        <div class="col_third" id="box_1" style="background-image:url(images/port_cont_2.jpg);">
            <a href="article1.php">
                <div class="fond">
                    <div class="fond_inner">
                        <div class="btn_article"><span>Architecture</span></div><br/>
                        <span class="titre_accueil">Un Portugal très contemporain</span><br/>
                        <span class="sous-titre_accueil">Par Christian Simenc</span><br/>
                        <span>le 21/08/2015</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col_two_third">
            <div class="row_full">
                <div class="col_half" id="box_2" style="background-image:url(images/art2_thumb3.jpg);">
                    <a href="article2.php">
                        <div class="fond">
                            <div class="fond_inner">
                                <div class="btn_article"><span>Décoration</span></div><br/>
                                <span class="titre_accueil">Une maison invisible</span><br/>
                                <span class="sous-titre_accueil">par Aude De La Conté</span><br/>
                                <span>le 08/11/2015</span>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col_half" id="box_3" style="background-image:url(images/art4_thumb1.png);">
                    <a href="article4.php">
                        <div class="fond">
                            <div class="fond_inner">
                                <div class="btn_article"><span>Artisanat</span></div><br/>
                                <span class="titre_accueil">São João da Madeira et le tourisme industriel</span><br/>
                                <span class="sous-titre_accueil">par Renaud Legrand</span><br/>
                                <span>le 22/04/2016</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row_full" id="box_4" style="background-image:url(images/art3_thumb3.jpg);">
                <a href="article3.php">
                    <div class="fond">
                        <div class="fond_inner">
                            <div class="btn_article"><span>Architecture</span></div><br/>
                            <span class="titre_accueil">Le Palacio Belmonte de Lisbonne</span><br/>
                            <span class="sous-titre_accueil">par Renaud Legrand</span><br/>
                            <span>le 17/03/2016</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div id="second_part">
        <div class="col_two_third">
            <div class="row_full">
                <div class="col_half" id="box_5" style="background-image:url(images/art2_thumb4.jpg);">
                    <a href="article2.php">
                        <div class="fond">
                            <div class="fond_inner">
                                <div class="btn_article"><span>Décoration</span></div><br/>
                                <span class="titre_accueil">Une maison invisible</span><br/>
                                <span class="sous-titre_accueil">par Aude De La Conté</span><br/>
                                <span>le 08/11/2015</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col_half" id="box_6" style="background-image:url(images/port_cont_5.jpg);">
                    <a href="article1.php">
                        <div class="fond">
                            <div class="fond_inner">
                                <div class="btn_article"><span>Architecture</span></div><br/>
                                <span class="titre_accueil">Un Portugal très contemporain</span><br/>
                                <span class="sous-titre_accueil">par Christian Simenc</span><br/>
                                <span>le 21/08/2015</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row_full" id="box_7" style="background-image:url(images/art3_thumb1.jpg);">
                <a href="article3.php">
                    <div class="fond">
                        <div class="fond_inner">
                            <div class="btn_article"><span>Architecture</span></div><br/>
                            <span class="titre_accueil">Le Palacio Belmonte de Lisbonne</span><br/>
                            <span class="sous-titre_accueil">par Renaud Legrand</span><br/>
                            <span>le 17/03/2016</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col_third" id="box_8" style="background-image:url(images/art4_thumb.png);">
            <a href="article4.php">
                <div class="fond">
                    <div class="fond_inner">
                        <div class="btn_article"><span>Artisanat</span></div><br/>
                        <span class="titre_accueil">São João da Madeira et le tourisme industriel</span><br/>
                        <span class="sous-titre_accueil">par Renaud Legrand</span><br/>
                        <span>le 22/04/2016</span>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div id="third_part">
        <div class="col_third" id="box_9" style="background-image:url(images/art2_thumb4.jpg);">
            <a href="article2.php">
                <div class="fond">
                    <div class="fond_inner">
                        <div class="btn_article"><span>Décoration</span></div><br/>
                        <span class="titre_accueil">Une maison invisible</span><br/>
                        <span class="sous-titre_accueil">par Aude De La Conté</span><br/>
                        <span>le 08/11/2015</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col_two_third">
            <div class="row_full">
                <div class="col_half" id="box_10" style="background-image:url(images/art4_thumb1.png);">
                    <a href="article4.php">
                        <div class="fond">
                            <div class="fond_inner">
                                <div class="btn_article"><span>Artisanat</span></div><br/>
                                <span class="titre_accueil">São João da Madeira et le tourisme industriel</span><br/>
                                <span class="sous-titre_accueil">par Renaud Legrand</span><br/>
                                <span>le 22/04/2016</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col_half" id="box_11" style="background-image:url(images/art3_thumb2.jpg);">
                    <a href="article3.php">
                        <div class="fond">
                            <div class="fond_inner">
                                <div class="btn_article"><span>Architecture</span></div><br/>
                                <span class="titre_accueil">Le Palacio Belmonte de Lisbonne</span><br/>
                                <span class="sous-titre_accueil">par Renaud Legrand</span><br/>
                                <span>le 17/03/2016</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row_full" id="box_12" style="background-image:url(images/port_cont_6.jpg);">
                <a href="article1.php">
                    <div class="fond">
                        <div class="fond_inner">
                            <div class="btn_article"><span>Architecture</span></div><br/>
                            <span class="titre_accueil">Un Portugal très contemporain</span><br/>
                            <span class="sous-titre_accueil">par Christian Simenc</span><br/>
                            <span>le 21/08/2015</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <div id="social_div">
        <div class="inner">
            <div id="instagram">
                <span class="sous-titre">Janela sur Instagram</span>
            </div>
            <div id="pinterest">
                <span class="sous-titre">Janela sur Pinterest</span>
            </div>
            <div id="twitter">
                <span class="sous-titre">Janela sur Twitter</span>
            </div>
        </div>
    </div>
</div>

<?php
require_once '_footer.html';
?>

<?php
require_once '_copyright.html';
?>
<?php
require_once '_scripts.html';
?>
</body>
</html>