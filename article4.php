<!doctype html>
<html>
<?php
require_once '_head.html';
?>
<body>
<?php
require_once '_header.html'
?>

<?php
require_once '_header_slide.html';
?>

<?php
require_once '_menu_slide.html';
?>

<div class="content">
	<div class="article">
		<div class="header_article" style="background-image: url(images/art4_img1.png);">
			<span class="titre_article">São João da Madeira et le tourisme industriel</span><br/>
			<span class="redac_article">Par Renaud Legrand</span><br/>
			<span>le 22 avril 2016</span><br/><br/>
			<span class="redac_article" style="font-size: 20px;">Durée : 10 minutes</span>
		</div>
		<div class="inner_article">
				<span class="chapeau">
					Il s'agit de la plus petite commune du Portugal. Elle possède un lien fort avec l'industrie de la chaussure. Mais vous y trouverez aussi des chapeaux, des crayons et une population travailleuse, souriante et très accueillante.<br/><br/>
				</span>

				<p>
					Pour mieux découvrir São João da Madeira et son patrimoine industriel, commencez par visiter le Welcome Center (http://www.turismoindustrial.cm-sjm.pt) situé dans la Tour da Oliva. Dans cet espace, installé dans une ancienne usine de métallurgie, vous pourrez obtenir plus d’informations sur les circuits du patrimoine industriel de la commune, programmer des visites aux usines et demander les services d’un guide touristique spécialisé, ou bien le guide multimédia des usines et institutions à visiter.
				</p>
				<p>
					Avant de découvrir ce patrimoine industriel, consultez la table interactive, qui vous indique les circuits existants et observez attentivement les flèches chronologiques qui résument l'histoire de l'usine Oliva et de l'entreprise Viarco (situé à proximité et que vous pouvez aussi visiter).
				</p>
				<p>
					Si vous souhaitez en apprendre plus sur les crayons Viarco, rien de mieux que de se rendre dans l'unique usine de crayons du pays, encore en activité. Dans la ville, les murs de Viarco sont, depuis 1931, recouverts d’archives publicitaires pour les produits de l'entreprise, et l'odeur de graphite qui flotte dans l’air rappelle avec nostalgie l'école primaire.
				</p>
				<p>
					Découvrez ensuite les métiers à tisser d'Heliotêxtil, usine où sont fabriquées des étiquettes de vêtement portant les instructions de lavage pour des marques de renommée internationale, des ceintures de sécurité pour les voitures ou bien encore des bandoulières des sacs de sport. Découvrez le processus de fabrication des rubans, étiquettes et passementeries et découvrez avec surprise comment un ensemble de milliers de fils peut donner naissance à des modèles si complexes.
				</p>

				<span class="sous-titre">La renommée de la chaussure portugaise</span><br/><br/>

				<p>
					La chaussure est l'un des produits portugais qui s'exportent le mieux, et São João da Madeira représente le centre de production de la chaussure portugaise. Nous vous invitons à commencer la visite par l'espace théorique, scientifique et de formation à l'industrie de la chaussure.
				</p>

				<p>
					C'est au Centre de formation professionnelle de l'industrie de la chaussure que sont formés bon nombre de futurs créateurs portugais. Le centre jouit d'ailleurs d'une reconnaissance internationale grâce aux prix gagnés par les élèves lors de divers concours. Vous aurez là une opportunité unique de visiter les espaces de formation, laboratoires et unités de production qui sont une parfaite copie des véritables usines du secteur. Saviez-vous que c’est l'unique école où les élèves disposent de toutes les ressources nécessaires à la création d'une chaussure du début à la fin ? Si vous souhaitez acquérir une connaissance théorique de l'industrie de la chaussure, le centre met à disposition des informations sur l'évolution de la chaussure au fil du temps.
				</p>

				<p>
					Pour approfondir vos connaissances techniques, visitez le Centre technologique de la chaussure, espace de formation dédié aux professionnels de la chaussure. Une équipe de techniciens y contrôle, en laboratoire, la qualité des nouveaux produits et équipements, et réalisent des tests de résistance des matériaux et de confort du produit final.
				</p>

				<p>
					Après la formation et la recherche, passez à l'action ! Helsar, l'une des plus importantes entreprises portugaises de chaussure, fabrique des modèles pour femme de renommée internationale. Si vous aimez les chaussures, préparez-vous à passer des heures dans le musée de l'usine, où sont exposés les modèles emblématiques de chaque collection, depuis les débuts de production de l'usine en 1979. L'entrepôt, que vous pouvez aussi visiter, compte des centaines de peaux, prêtes à être transformées en chaussures pour la prochaine collection de l'entreprise, après un passage par la ligne de production qui requiert un travail manuel de grande précision.
				</p>

				<p>
					Passez des talons hauts aux chaussures pour homme et chaussures artisanales d'Evereste. Malgré la production de quelques modèles féminins, Evereste se consacre à la chaussure pour homme, pour son compte ou pour d'autres marques et stylistes qui lui confient leurs productions. Dans la salle d'exposition de l'entreprise, vous pourrez admirer des modèles de chaussures classiques ou plus osés, prêts à être personnalisés au goût du client.
				</p>

				<span class="sous-titre">Chapeaux: de São João da Madeira vers le monde entier</span><br/><br/>

				<p>
					Saviez-vous que la production du feutre des chapeaux de la police britannique féminine se fait à São João da Madeira ? L'entreprise Fepsa (http://www.fepsa.pt) est l'une des entreprises les plus importantes en matière de production de poils, transformé ensuite en feutre pour les chapeaux. Au cours de la visite, vous pourrez parcourir toutes les phases de production puis finir par la visite de l'entrepôt qui conserve le feutre fini, aligné, sous toutes ses teintes.
				</p>

				<p>
					Une grande partie de la matière première utilisée par Fepsa provient de la société Cortadoria Nacional de Pêlo. Cette usine transforme les poils d'animaux provenant d'espèces non menacées pour les revendre, en masse, aux secteurs de l'habillement. Il n'est possible de visiter que certaines parties de l'usine.
				</p>

				<p>
					Du poil au feutre, du feutre au chapeau: tel est le processus de fabrication d'un chapeau. Bien qu'elle ait été, dans la première moitié du XXème siècle, l'industrie la plus importante de São João da Madeira, on n’y fabrique plus de chapeaux.
				</p>

				<p>
					Le Musée de la Chapellerie, unique en son genre dans la Péninsule ibérique, préserve l'histoire de cette industrie et confectionne encore quelques chapeaux sur demande. Le musée expose des machines et des outils de l'industrie et, bien sûr, de nombreux modèles de chapeaux de différentes époques.
				</p>

				<p>
					La chaussure est l'un des produits portugais qui s'exportent le mieux, et São João da Madeira représente le centre de production de la chaussure portugaise. Nous vous invitons à commencer la visite par l'espace théorique, scientifique et de formation à l'industrie de la chaussure.
				</p>

				<span>Source : <a href="http://fr.visitportoandnorth.travel/Porto-et-le-Nord/Visiter/Artigos/Sao-Joao-da-Madeira-et-le-tourisme-industriel">fr.visitportoandnorth.travel</span></a>
		</div>
	</div>
</div>

<?php
require_once '_footer.html';
?>

<?php
require_once '_copyright.html';
?>
<?php
require_once '_scripts.html';
?>
</body>
</html>