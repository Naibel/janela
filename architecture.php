<!doctype html>
<html>
<?php
require_once '_head.html';
?>
<body>
<?php
require_once '_header.html'
?>

<?php
require_once '_header_slide.html';
?>

<?php
require_once '_menu_slide.html';
?>

<div class="content">
    <div class="titre_rubrique">
        <span class="titre">Architecture</span>
    </div>
    <div class="content_inner">
        <span class="titre">En une</span><br/>
        <div class="ligne"></div>
        <div class="main_article">
            <a href="article1.php"><div class="main_article_img" style="background-image:url(images/art1_thumb2.jpg);"></div></a>
            <div class="title_main_article">
                <span class="sous-titre">Un Portugal très contemporain</span><br/>
                <span class="sous-titre" style="font-size: 20px;">Par Christian Simenc - le 21 août 2015</span><br/>
            </div>
        </div>
        <span class="titre" style="font-size: 34px;">Autres articles</span><br/>
        <div class="ligne"></div>
        <div class="autres_articles">
            <div class="article_thumb">
                <a href="article3.php"><div class="article_thumb_img" style="background-image:url(images/art3_thumb3.jpg);"></div></a>
                <div class="title_thumb_article">
                    <span class="sous-titre" style="font-size: 20px;">Le Palacio Belmonte de Lisbonne</span><br/>
                    <span class="sous-titre" style="font-size: 14px;">Par Renaud Legrand - le 17 mars 2016</span><br/>
                </div>
            </div>
            <div class="article_thumb">
                <a href="article.php"><div class="article_thumb_img"></div></a>
                <div class="title_thumb_article">
                    <span class="sous-titre" style="font-size: 20px;">Article de la sous-partie</span><br/>
                    <span class="sous-titre" style="font-size: 14px;">Par Machin Chose - le 16 mars 2016</span><br/>
                </div>
            </div>
            <div class="article_thumb">
                <a href="article.php"><div class="article_thumb_img"></div></a>
                <div class="title_thumb_article">
                    <span class="sous-titre" style="font-size: 20px;">Article de la sous-partie</span><br/>
                    <span class="sous-titre" style="font-size: 14px;">Par Machin Chose - le 16 mars 2016</span><br/>
                </div>
            </div>
            <div class="article_thumb">
                <a href="article.php"><div class="article_thumb_img"></div></a>
                <div class="title_thumb_article">
                    <span class="sous-titre" style="font-size: 20px;">Article de la sous-partie</span><br/>
                    <span class="sous-titre" style="font-size: 14px;">Par Machin Chose - le 16 mars 2016</span><br/>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require_once '_footer.html';
?>

<?php
require_once '_copyright.html';
?>
<?php
require_once '_scripts.html';
?>

</body>
</html>