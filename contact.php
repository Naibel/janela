<!doctype html>
<html>
<?php
require_once '_head.html';
?>
<body>
<?php
require_once '_header.html'
?>

<?php
require_once '_header_slide.html';
?>

<?php
require_once '_menu_slide.html';
?>

<div class="content">
	<div class="article">
		<div class="header_other">
			<span class="titre_article">Contact</span><br/>
		</div>
		<div class="inner_article">
			<fieldset border="0" style="margin-top:0px; text-align:center;"><!--FORMULAIRE DE CONTACT-->
				<form enctype="multipart/form-data" action="envoi_mail.php" method="post">
					<input type="email" placeholder="Votre adresse e-mail" required name="adresse"/>
					<input type="text" placeholder="Objet" required name="nom"/>
					<textarea required name="REM">Votre message</textarea> <!--Formulaire de remarques-->
					<input class="envoi" type="submit" value="Envoyer"/>
				</form>
			</fieldset>
		</div>
	</div>
</div>

<?php
require_once '_footer.html';
?>

<?php
require_once '_copyright.html';
?>
<?php
require_once '_scripts.html';
?>

</body>
</html>