<!doctype html>
<html>
<?php
    require_once '_head.html';
?>
<body>
<?php
    require_once '_header.html'
?>

<?php
    require_once '_header_slide.html';
?>

<div class="content">
    <div id="first_part">
        <div class="col_third" id="box_1">
            <a href="article.php">
                <div class="fond">
                    <div class="fond_inner">
                        <div class="btn_article"><span>Architecture</span></div><br/>
                        <span class="titre_accueil">L'histoire de l'architecture portugaise</span><br/>
                        <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                        <span>le 17/03/2016</span>
                    </div>
                </div>
            </a>
        </div>
        <div class="col_two_third">
            <div class="row_full">
                <div class="col_half" id="box_2">
                    <a href="article.php">
                        <div class="fond">
                            <div class="fond_inner">
                                <div class="btn_article"><span>Artisanat</span></div><br/>
                                <span class="titre_accueil">Les lutiers de Lisbonne</span><br/>
                                <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                                <span>le 17/03/2016</span>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col_half" id="box_3">
                    <a href="article.php">
                        <div class="fond">
                            <div class="fond_inner">
                                <div class="btn_article"><span>Architecture</span></div><br/>
                                <span class="titre_accueil">L'autre versant de l'architecture</span><br/>
                                <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                                <span>le 17/03/2016</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="row_full" id="box_4">
                <a href="article.php">
                    <div class="fond">
                        <div class="fond_inner">
                            <div class="btn_article"><span>Design</span></div><br/>
                            <span class="titre_accueil">Les dernières tendances</span><br/>
                            <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                            <span>le 17/03/2016</span>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div id="second_part">
        <div class="col_two_third">
            <div class="row_full">
                <div class="col_half" id="box_5">
                    <div class="fond">
                        <div class="fond_inner">
                            <div class="btn_article"><span>Architecture</span></div><br/>
                            <span class="titre_accueil">Les lutiers de Lisbonne</span><br/>
                            <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                            <span>le 17/03/2016</span>
                        </div>
                    </div>
                </div>
                <div class="col_half" id="box_6">
                    <div class="fond">
                        <div class="fond_inner">
                            <div class="btn_article"><span>Architecture</span></div><br/>
                            <span class="titre_accueil">L'autre versant de l'architecture</span><br/>
                            <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                            <span>le 17/03/2016</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_full" id="box_7">
                <div class="fond">
                    <div class="fond_inner">
                        <div class="btn_article"><span>Architecture</span></div><br/>
                        <span class="titre_accueil">Les monuments historiques</span><br/>
                        <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                        <span>le 17/03/2016</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col_third" id="box_8">
            <div class="fond">
                <div class="fond_inner">
                    <div class="btn_article"><span>Architecture</span></div><br/>
                    <span class="titre_accueil">Les dernières tendances</span><br/>
                    <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                    <span>le 17/03/2016</span>
                </div>
            </div>
        </div>
    </div>
    <div id="third_part">
        <div class="col_third" id="box_9">
            <div class="fond">
                <div class="fond_inner">
                    <div class="btn_article"><span>Architecture</span></div><br/>
                    <span class="titre_accueil">L'héritage de Fernando Tavora</span><br/>
                    <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                    <span>le 17/03/2016</span>
                </div>
            </div>
        </div>
        <div class="col_two_third">
            <div class="row_full">
                <div class="col_half" id="box_10">
                    <div class="fond">
                        <div class="fond_inner">
                            <div class="btn_article"><span>Architecture</span></div><br/>
                            <span class="titre_accueil">Les monuments historiques</span><br/>
                            <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                            <span>le 17/03/2016</span>
                        </div>
                    </div>
                </div>
                <div class="col_half" id="box_11">
                    <div class="fond">
                        <div class="fond_inner">
                            <div class="btn_article"><span>Architecture</span></div><br/>
                            <span class="titre_accueil">Les lutiers de Lisbonne</span><br/>
                            <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                            <span>le 17/03/2016</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_full" id="box_12">
                <div class="fond">
                    <div class="fond_inner">
                        <div class="btn_article"><span>Architecture</span></div><br/>
                        <span class="titre_accueil">L'histoire de l'architecture portugaise</span><br/>
                        <span class="sous-titre_accueil">par Marcel Machin</span><br/>
                        <span>le 17/03/2016</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="social_div">
        <div class="inner">
            <div id="instagram">
                <span class="sous-titre">Janela sur Instagram</span>
            </div>
            <div id="pinterest">
                <span class="sous-titre">Janela sur Pinterest</span>
            </div>
            <div id="twitter">
                <span class="sous-titre">Janela sur Twitter</span>
            </div>
        </div>
    </div>
</div>

<?php
require_once '_footer.html';
?>

<?php
require_once '_copyright.html';
?>
<?php
require_once '_scripts.html';
?>
</body>
</html>