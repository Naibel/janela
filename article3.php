<!doctype html>
<html>
<?php
require_once '_head.html';
?>
<body>
<?php
require_once '_header.html'
?>

<?php
require_once '_header_slide.html';
?>

<?php
require_once '_menu_slide.html';
?>

<div class="content">
	<div class="article">
		<div class="header_article" style="background-image: url(images/art3_thumb1.jpg);">
			<span class="titre_article">Le Palacio Belmonte de Lisbonne</span><br/>
			<span class="redac_article">Par Renaud Legrand</span><br/>
			<span>le 17 mars 2016</span><br/><br/>
			<span class="redac_article" style="font-size: 20px;">Durée : 5 minutes</span>
		</div>
		<div class="inner_article">
				<span class="chapeau">
					« Avril au Portugal, à deux c’est idéal… » Avril est passé et avec lui le refrain Tsf. Mais aucunement l’envie d’être à Lisbonne. Toutes les occasions sont donc bonnes, poser sa valise au Palacio Belmonte en est une excellente. C’est un palais – lettres de noblesse : ayant appartenu aux comtes de Belmonte pendant 500 ans –, c’est un hôtel – 5 étoiles à son blason – mais avec le charme d’une demeure hors du temps.<br/><br/>
				</span>

			<p>
				Ressuscité en 2003, le lieu, situé sous le château Saint-Georges dans le quartier de l’Alfama aux airs de casbah lusitanienne, a retrouvé ses patines, ses fresques et ses azulejos (30 000 !). En tout, une dizaine de suites, sans télévision mais avec chacune son charme, fait de dépouillement et de luxe discret, fait d’éclectisme aussi – les propriétaires, français, ont la fibre chineuse et l’amour du bel objet, que celui-ci soit du XVe, du XVIIIe siècle, du Portugal, du Japon ou d’Afrique, beau classique ou créé par les plus contemporains des artistes… Auxquelles s’ajoutent un jardin d’ombre et de parfums, une piscine de marbre noir, des terrasses, des recoins, du silence. Et des terrasses, des terrasses avec la ville et le Tage à perte de vue. Le souvenir de ce séjour vous fera découvrir ce que veut dire le mot saudade.
			</p>

			<p>
				Palacio Belmonte, Pátio Dom Fradique 14  1100-624 Lisbonne. www.palaciobelmonte.com
			</p>

			<img src="images/art3_img1.jpg"><div class="ligne"></div>

			<img src="images/art3_img2.jpg"><div class="ligne"></div>

			<img src="images/art3_img3.jpg"><div class="ligne"></div>

			<img src="images/art3_img4.jpg"><div class="ligne"></div>

			<img src="images/art3_img5.jpg"><div class="ligne"></div>

			<span>Source : <a href="http://www.admagazine.fr/lieux/articles/le-palacio-belmonte-de-lisbonne/764#lbIS5qMGzXzKwvkW.99">AD Magazine</span></a>
		</div>
	</div>
</div>

<?php
require_once '_footer.html';
?>

<?php
require_once '_copyright.html';
?>
<?php
require_once '_scripts.html';
?>
</body>
</html>