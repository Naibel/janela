$(document).ready(function(){
	var pos_head = $('.content').offset().top;
	$('.menu_slide').hide();
	$('.header_slide').hide();

    $(window).scroll(function() {
        if ($(window).scrollTop() >= pos_head) {
            $('.header_slide').slideDown("fast");
        } else {
            $('.header_slide').slideUp("fast");
            $('.menu_slide').hide();
        }
    });	

    $('.menu').click(function(){
			$('.menu_slide').slideToggle(200);
	});
});